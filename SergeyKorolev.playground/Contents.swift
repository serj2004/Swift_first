import Foundation
import UIKit

//MARK: - ДЗ1

var greeting = "Sergey Korolev"

print(greeting)

//MARK: - ДЗ2

//Задача 1
var milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

//Задача 2
var milkPrice: Double = 3

//Задача 3
milkPrice = 4.20

//Задача 4
var milkBottleCount: Int?
milkBottleCount = 20
var profit: Double = 0.0
profit = Double(milkBottleCount!) * milkPrice
print(profit)

//Дополнительное задание*
if let tempCount = milkBottleCount {
    print(tempCount)
}
/*Если попытаться извлечь опциональное значение с помощью
 print(milkBottleCount!) не проверив её на nil произойдет сбой
 программы.*/

//Задача 5
var employeesList = [String]()
employeesList += ["Иван", "Петр", "Геннадий", "Марфа", "Андрей"]

//Задача 6
var isEveryoneWorkHard: Bool = false
var workingHours = 39
if workingHours >= 40{
    isEveryoneWorkHard = true
}
else{
    isEveryoneWorkHard = false
}
print(isEveryoneWorkHard)

//MARK: - ДЗ3
//1 Задание
func makeBuffer() -> (String) -> Void {
    var box: String = ""
    
    return {
        (arg: String) in
        if arg.isEmpty {
            print(box)
        } else {
            box += arg
        }
    }
}
let buffer = makeBuffer()

//2 Задание
func checkPrimeNumber(num: Int) -> Bool {
    guard num >= 2 else {return false}
    
    for index in 2 ..< num {
        if num % index == 0 {
            return false
        }
    }
    return true
}
